var express = require('express');
var userFile = require('./user.json');
var app = express();
var port = process.env.PORT || 3500;
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var URLbase = '/apiperu/v1/';
var baseMLabURL = 'https://api.mlab.com/api/1/databases/apiperudbjulio/collections';
const apiKeyMLab = 'apiKey=VYeDZlB_MswQLOlU5bgWcOIODLlJHPd_';

app.use(bodyParser.json());

//LOGIN

app.post(URLbase + 'login', function(req, res) {
  console.log('ENTRO');
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" + query + "&l=1&" + apiKeyMLab)
  //tmb hay una opcion mandandole el_id directo en la url sin query parameters
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Login ok
      {
        clienteMlab = requestJSON.createClient(baseMLabURL + "/user")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apiKeyMLab, JSON.parse(cambio), function(errP, resP, bodyP) {
        res.send({"login":"ok", "id":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})
        })
      } else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

//LOGOUT

app.post(URLbase + 'logout', function(req, res) {
    var id = req.headers.id

    var query = 'q={"userID":' + id + ', "logged":true}'
    clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" + query + "&l=1&" + apiKeyMLab)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // Estaba logado
        {
          clienteMlab = requestJSON.createClient(baseMLabURL + "/user")
          var cambio = '{"$set":{"logged":false}}'
          clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apiKeyMLab, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"logout":"ok", "id":body[0].userID})
          })

        }
        else {
          res.status(200).send('Usuario no logado previamente')
        }
      }
    })
})

//GET users consumiendo API REST de mLab

app.get(URLbase + 'users',
  function(req, res){
    console.log('GET /apiperu/v1/');
    var filter = 'f={"_id":0}';
    var clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" +filter +"&"+ apiKeyMLab)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        res.send(body)
      }
    })
    console.log('Cliente HTML mLAb creado');
  }

)

//DELETE USER CON ID DIRECTO

// app.delete(URLbase + 'users/:id',
//   function(req, res){
//     console.log('GET /apiperu/v1/');
//     let usuarioElegido = req.params.id;
//     var clienteMlab = requestJSON.createClient(baseMLabURL + "/user/"+usuarioElegido+"?"+ apiKeyMLab)
//     clienteMlab.delete('', function(err, resM, body) {
//       if (!err) {
//         res.send(body)
//       }
//     })
//     console.log('Cliente HTML mLAb creado');
//   }
// )

//DELETE USER CON GET PREVIO

app.delete(URLbase + 'users/:id',
  function(req, res){
    console.log('GET /apiperu/v1/');
    let usuarioElegido = req.params.id;
    var query = 'q={"userID":' + usuarioElegido + '}';
    var clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" +usuarioElegido +"&"+ apiKeyMLab)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        let idMongo = body[0]._id.$oid;
        var clienteMlabDelete = requestJSON.createClient(baseMLabURL + "/user/"+idMongo+"?"+ apiKeyMLab)
        clienteMlabDelete.delete('', function(err, resM, body) {
          if (!err) {
            res.send(body)
          }
        })
      }
    })

    console.log('Cliente HTML mLAb creado');
  }
)

app.put(URLbase + 'users/:id',
function(req, res) {
  var id = req.params.id;
  var queryStringID = 'q={"userID":' + id + '}&';
  var clienteMlab = requestJSON.createClient(baseMLabURL+'/');
  //console.log('/user?'+ queryStringID + apiKeyMLab);
  clienteMlab.get('user?'+ queryStringID + apiKeyMLab ,
    function(error, respuestaMLab , body) {
      console.log(body);
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     let idMongo = body[0]._id.$oid;
//httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
     clienteMlab.put(baseMLabURL + '/user/' + idMongo + '?' + apiKeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body);
       res.send(body);
      });
 });
});

// Petición PUT con id de mLab (_id.$oid)
app.put(URLbase + 'usersmLab/:id',
  function (req, res) {
    var id = req.params.id;
    let userBody = req.body;
    var queryString = 'q={"id":' + id + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);

    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body){
        let response = body[0];
        console.log(body);

        //Actualizo los campos del usuario
        let updatedUser = {
          "id" : req.body.id,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };//Otra forma simplificada (para muchas propiedades)
        // var updatedUser = {};
        // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
        // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);

        //Llamo al put de mlab.
        httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                response = {
                  "msg" : "Error actualizando usuario."
                }
                res.status(500);
            } else {
              if(body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "Usuario actualizado correctamente."
                }
                res.status(404);
              }
            }
            res.send(response);
          });
      });
});

app.listen(port);
