var express = require('express');
var userFile = require('./user.json');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
var URLbase = '/apiperu/v1/';

app.use(bodyParser.json());

//GET USERS
// app.get(URLbase + 'users',
//   function(req, res){
//     console.log('GET /apiperu/v1/users');
//     res.send(userFile);
// });

// GET users por ID

app.get(URLbase + 'users/:id',
  function(req, res){
      console.log('GET /apiperu/v1/users/:id');
      console.log(req.params);
      console.log(req.params.id);
      let pos = req.params.id;
      //usuario encontrado
      let usrEncontrado = userFile[pos-1];

      if(usrEncontrado==undefined){
        console.log(usrEncontrado);
        res.status(404);
        res.send({"msg":"usuario no encontrado"})
      }
      res.send(userFile[pos-1]);
  })

  // // GET con query string
  //
  // app.get(URLbase + 'users',
  //   function(req, res){
  //       console.log(req.query);
  //       console.log(req.query.id);
  //   })

  // GET con query string

  app.get(URLbase + 'users/:id/:id2',
    function(req, res){
        console.log(req.query);
        console.log(req.params);
    })

// POST users
app.post(URLbase + 'users',
  function(req, res){
    console.log(req.body);
    console.log('POST apiperu/v1/users');
    let newID = userFile.length + 1;
    let newUser = {
      "userID" : newID,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    }
    userFile.push(newUser);
    console.log(userFile);
    res.status(201);
    res.send({"msg":"Usuario añadido correctamente", newUser});
  })

  // DELETE users
  app.delete(URLbase + 'users/:id',
    function(req, res){
      console.log(req.body);
      console.log('DELETE apiperu/v1/users/:id');
      let removeID = req.params.id - 1;
      let userFileNew = userFile.splice(removeID,1);
      console.log(userFile);

      if(userFileNew.length==userFileNew.length){
        res.status(404);
        res.send({"msg":"usuario no encontrado"})
      }

      res.status(201);
      res.send({"msg":"Usuario eliminado correctamente", removeID});
    })

    // PUT users
    app.put(URLbase + 'users/:id',
      function(req, res){
        console.log(req.body);
        console.log('PUT apiperu/v1/users/:id');
        let posID = req.params.id - 1;

        if(userFile[posID]==null || userFile[posID]==''){
          res.status(404);
          res.send({"msg":"usuario no encontrado"})
        }

        userFile[posID].first_name=req.body.first_name;
        userFile[posID].last_name=req.body.last_name;
        userFile[posID].email=req.body.email;
        userFile[posID].password=req.body.password;
        let usrActualizado=userFile[posID];
        console.log(usrActualizado);

        res.status(201);

        res.send({"msg":"Usuario actualizado correctamente", usrActualizado});
      })

app.listen(port);
