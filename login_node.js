var express = require('express');
var userFile = require('./user.json');
var app = express();
var port = process.env.PORT || 3500;
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var URLbase = '/apiperu/v1/';
var baseMLabURL = 'https://api.mlab.com/api/1/databases/apiperudbjulio/collections';
const apiKeyMLab = 'apiKey=VYeDZlB_MswQLOlU5bgWcOIODLlJHPd_';

app.use(bodyParser.json());

//LOGIN

app.post(URLbase + 'login', function(req, res) {
  console.log('ENTRO');
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" + query + "&l=1&" + apiKeyMLab)
  //tmb hay una opcion mandandole el_id directo en la url sin query parameters
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Login ok
      {
        clienteMlab = requestJSON.createClient(baseMLabURL + "/user")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apiKeyMLab, JSON.parse(cambio), function(errP, resP, bodyP) {
        res.send({"login":"ok", "id":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})
        })
      } else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

//LOGOUT

app.post(URLbase + 'logout', function(req, res) {
    var id = req.headers.id

    var query = 'q={"userID":' + id + ', "logged":true}'
    clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" + query + "&l=1&" + apiKeyMLab)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1) // Estaba logado
        {
          clienteMlab = requestJSON.createClient(baseMLabURL + "/user")
          var cambio = '{"$set":{"logged":false}}'
          clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apiKeyMLab, JSON.parse(cambio), function(errP, resP, bodyP) {
              res.send({"logout":"ok", "id":body[0].userID})
          })

        }
        else {
          res.status(200).send('Usuario no logado previamente')
        }
      }
    })
})

app.listen(port);
